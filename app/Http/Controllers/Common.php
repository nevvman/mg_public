<?php

namespace App\Http\Controllers;

use InstagramAPI\Instagram;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use Illuminate\Http\Request;

class Common extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


	/**
	 * Create new instagram instance
	 *
	 * @param string $inst_name Instagram name (optional)
	 * @param string $password Instagram password (optional)
	 * @return \App\Http\Controllers\Instagram
	 */
	public static function newInstagram($inst_name = '', $password = ''){
		$i = new Instagram(false, true, [
			"storage" => "mysql",
			"dbhost" => env('DB_HOST', '192.168.1.100'),
			"dbname" => env('DB_DATABASE', 'man'),
			"dbusername" => env('DB_USERNAME', 'forge'),
			"dbpassword" => env('DB_PASSWORD', ''),
		]);

		if (!empty($inst_name) && !empty($password)){
			$i->setUser($inst_name, $password);
			$i->login();
		}

		return $i;
	}

	/**
	 * Set random Instagram account for queries
	 *
	 * @param $i \App\Http\Controllers\Instagram
	 * @return \App\Http\Controllers\Instagram
	 */
	public static function setRandomInstagramUser($i){
		set_time_limit(120);

		// try new users
		$instagram_accounts = DB::table('user_instagram')->where('status', '')->where('inst_name', '<>', $i->username)->get()->toArray();

		// if no users, then user is the same - sleep
		if(empty($instagram_accounts)){
			sleep(random_int(1, 3));
		}

		while (!empty($instagram_accounts)) {
			$random_key = array_rand($instagram_accounts);
			$random_account = $instagram_accounts[$random_key];

			try{
				$i->setUser($random_account->inst_name, $random_account->password);
				$i->login();
				$instagram_accounts = [];
			} catch (\Exception $e) {
				unset($instagram_accounts[$random_key]);
				DB::table('user_instagram')->where('id', $random_account->id)->update(['status' => $e->getMessage()]);
			}
		}

		return $i;
	}

	/**
	 * Get my followers
	 *
	 * @param type $i \App\Http\Controllers\Instagram
	 * @return array Array of users info by pk
	 */
	public static function getMyFollowers($i){

		$maxId = null;
		$followers = [];
		$followers_by_pk = [];

		do {
			$response = $i->getSelfUserFollowers($maxId);
			$followers = array_merge($followers, $response->getUsers());
			$maxId = $response->getNextMaxId();
		} while ($maxId !== null);

		foreach ($followers as $user){
			$followers_by_pk[$user->pk] = [
				'name' => $user->username,
				'picture' => $user->profile_pic_url,
				'full_name' => $user->full_name,
			];
		}

		return $followers_by_pk;
	}

	/**
	 * Get my followings
	 *
	 * @param type $i \App\Http\Controllers\Instagram
	 * @return array Array of users info by pk
	 */
	public static function getMyFollowings($i){

		$maxId = null;
		$following = [];
		$following_by_pk = [];

		do {
			$response = $i->getSelfUsersFollowing($maxId);
			$following = array_merge($following, $response->getUsers());
			$maxId = $response->getNextMaxId();
		} while ($maxId !== null);

		foreach ($following as $user){
			$following_by_pk[$user->pk] = [
				'name' => $user->username,
				'picture' => $user->profile_pic_url,
				'full_name' => $user->full_name,
			];
		}

		return $following_by_pk;
	}

	/**
	 *
	 *
	 */
	public static function getAccountsMenu(){
		if (!Auth::check()) {
			return false;
		}

		$user_instagram = DB::table('user_instagram')->where('user_id', Auth::user()->id)->get();

		if(empty($user_instagram)){
			return false;
		}

		foreach ($user_instagram as $account){
			$accounts[$account->id] = $account;
		}

		if(isset($_COOKIE['current_account'])){
			$current_account = $_COOKIE['current_account'];
		}else{
			$current_account = $user_instagram[0]->id;
			setcookie('current_account', $user_instagram[0]->id);
		}

		$current_account_image = '';

		if(!isset($_COOKIE['current_account_image'])){
			$i = Common::newInstagram();
			try{
				Common::setRandomInstagramUser($i);
				$user_info = $i->getUserInfoByName($accounts[$current_account]->inst_name);
				if($user_info->status == 'ok' && !empty($user_info->user->profile_pic_url)){
					$current_account_image = $user_info->user->profile_pic_url;
					setcookie('current_account_image', $current_account_image);
				}
			} catch (Exception $ex) {

			}
		}else{
			$current_account_image = $_COOKIE['current_account_image'];
		}

		return ['accounts' => $accounts, 'current_account_name' => $accounts[$current_account]->inst_name, 'current_account_id' => $current_account, 'current_account_image' => $current_account_image];
	}
}
