<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class Cron extends Controller {

	private $i;
	private $i_random;

	public function __construct() {
		$this->i = Common::newInstagram();
		$this->i_random = Common::newInstagram();
	}

	private function executeCronEvent($user_instagram_id, $type, $filter_arr) {
		set_time_limit(120);

		$filter = isset($filter_arr["filter"]) ? $filter_arr["filter"] : '';
		$user_filter = isset($filter_arr["additional_filter"]) ? $filter_arr["additional_filter"] : [];
		$source = isset($filter_arr["filter_source"]) ? $filter_arr["filter_source"] : '';
		$count = isset($filter_arr['count']) ? $filter_arr['count'] : 0;
		$message = isset($filter_arr["message"]) ? $filter_arr["message"] : '';
		$comment_text = isset($filter_arr["comment_text"]) ? $filter_arr["comment_text"] : '';

		$user = DB::table('user_instagram')->where('id', $user_instagram_id)->first();

		try {
			$this->i->setUser($user->inst_name, $user->password);
			$this->i->login();
		} catch (\Exception $e) {
			echo 'Something went wrong: ' . $e->getMessage() . "\n";
		}

		switch ($type) {
			case "Follow":
				if ($filter == "User") {
					$result = $this->followByUsername($source, $message);
				} elseif ($filter == "HashTag") {
					$result = $this->followByHashTag($source, $count, $message, $user_filter);
				} elseif ($filter == "Location") {
					$result = $this->followByLocation($source, $count, $message, $user_filter);
				} elseif ($filter == "Copy_account") {
					$result = $this->copy_followers_by_account($source, $count);
				}
				break;
			case "UnFollow":
				if ($filter == "User") {
					$result = $this->unfollowByUsername($source);
				}
				break;
			case "Like":
				if ($filter == "User") {
					$result = $this->likeByUsername($source, $count);
				} elseif ($filter == "HashTag") {
					$result = $this->likeByHashTag($source, $count, $user_filter);
				} elseif ($filter == "Location") {
					$result = $this->likeByLocation($source, $count, $user_filter);
				}
				break;
			case "Comment":
				if ($filter == "HashTag") {
					$result = $this->commentByHashTag($source, $count, $comment_text, $user_filter);
				} elseif ($filter == "Location") {
					$result = $this->commentByLocation($source, $count, $comment_text, $user_filter);
				}
				break;
			case "Autopost":
				$result = $this->autopost($filter_arr["path"], $filter_arr["text_photo"]);
				break;
		}

		return $result;
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
//		try {
//			$this->i->setUser('alexwin1', 'nika0906polsha');
//			$this->i->login();
//		} catch (\Exception $e) {
//			echo 'Something went wrong: ' . $e->getMessage() . "\n";
//		}
//
//		//$this->likeByHashTag('kyiv', 15);
//		$this->copy_followers_by_account("xkirpa", 300);
//		exit;

		$events = DB::table('events')->where('date_execute', '<', time())->where('progress', 'scheduled')->get();

		foreach ($events as $event) {
			DB::table('events')->where('id', $event->id)->update(['progress' => 'in progress']);
		}

		foreach ($events as $event) {
			$filter = unserialize($event->filter);
			$event_result = $this->executeCronEvent($event->user_instagram_id, $event->type, $filter);

			if($event_result['result'] === false){
				$progress = 'failed';
				$count = 0;
			}else{
				$progress = 'finished';
				$count = $event_result['result'];
			}

			DB::table('events')->where('id', $event->id)->update(['progress' => $progress, 'progress_count' => $count, 'progress_message' => $event_result['progress_message'], 'date_finished' => time()]);
		}

		//print_r($event_result);
	}

	/**
	 *
	 * @param type $source
	 * @param type $captionText
	 * @return type
	 */
	private function autopost ($source, $captionText = "") {
		$progress_message = '';

		try {
			$url = public_path() . "/" . $source["patch"] . "/" . $source["name"];
			//sleep(random_int(1, 4));

			$upload_response = $this->i->uploadTimelinePhoto($url, ['caption' => $captionText]);
			$result = $upload_response->status == 'ok' ? true : false;
			unlink($url);
		} catch (\Exception $e) {
			$result = false;
			$progress_message = $e->getMessage();
		}

		return ['result' => $result, 'progress_message' => $progress_message];
	}

	/**
	 *
	 * @param type $username
	 * @param type $message
	 * @return type
	 */
	private function followByUsername ($username, $message = ""){
		$result = false;
		$progress_message = '';

		try {
			Common::setRandomInstagramUser($this->i_random);
			$id = $this->i_random->getUsernameId($username);
			$this->i->follow($id);
			$result = true;

			if ($message !== "") {
				$this->i->directMessage($id, $message);
			}

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $result, 'progress_message' => $progress_message];
	}

	/**
	 *
	 * @param type $username
	 * @return boolean
	 */
	private function unfollowByUsername ($username){
		$result = false;
		$progress_message = '';

		try {
			Common::setRandomInstagramUser($this->i_random);
			$id = $this->i_random->getUsernameId($username);
			$this->i->unfollow($id);
			$result = true;

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $result, 'progress_message' => $progress_message];
	}

	/**
	 * Follow users by posts hashtag
	 *
	 * @param string $hashtag
	 * @param int $count_needed
	 * @return int Counter
	 */
	private function followByHashTag ($hashtag = '', $count_needed = 0, $text_message = "", $user_filter = []){
		$progress_message = '';

		if(empty($hashtag) || empty($count_needed)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		try {
			$following_by_pk = Common::getMyFollowings($this->i);
			$maxId = null;
			$count = 0;
			$follow_id = array();

			do {
				Common::setRandomInstagramUser($this->i_random);
				$feed = $this->i_random->getHashtagFeed($hashtag, $maxId);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if(!$feed_item->user->is_private && !in_array($feed_item->user->pk, array_keys($following_by_pk))){

						$following_by_pk[$feed_item->user->pk] = $feed_item->user->pk;
						if(!$this->additionalUserFilter($feed_item->user->pk, $user_filter)){
							continue;
						}

						$follow_response = $this->i->follow($feed_item->user->pk);
						if($follow_response->status == 'ok'){
							$count++;
							$follow_id[] = $feed_item->user->pk;
							sleep(random_int(1, 2));
						}
					}
				}

				$maxId = $feed->getNextMaxId();

			} while ($maxId !== null && $count < $count_needed);

			if ($text_message !== "") {
				sleep(random_int(4, 7));
				$this->i->directMessage($follow_id, $text_message);
			}
		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

	/**
	 * Follow users by posts location
	 *
	 * @param string $position
	 * @param int $count_needed
	 * @return int
	 */
	private function followByLocation($position = '', $count_needed = 0,$text_message = "", $user_filter = []) {
		$progress_message = '';

		if(empty($position) || empty($count_needed)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		list($lat, $lng) = explode(",", substr($position, 1, -1));

		try {
			Common::setRandomInstagramUser($this->i_random);
			$locations = $this->i_random->searchLocation($lat, $lng);
			$place_id = $locations->venues[0]->external_id;

			if(!$place_id){
				return ['result' => false, 'progress_message' => 'Empty params'];
			}

			$following_by_pk = Common::getMyFollowings($this->i);
			$maxId = null;
			$count = 0;
			$follow_id = array();

			do {
				Common::setRandomInstagramUser($this->i_random);
				$feed = $this->i_random->getLocationFeed($place_id, $maxId);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if(!$feed_item->user->is_private && !in_array($feed_item->user->pk, array_keys($following_by_pk))){

						$following_by_pk[$feed_item->user->pk] = $feed_item->user->pk;
						if(!$this->additionalUserFilter($feed_item->user->pk, $user_filter)){
							continue;
						}

						$follow_response = $this->i->follow($feed_item->user->pk);
						if($follow_response->status == 'ok'){
							$count++;
							$follow_id[] = $feed_item->user->pk;
							sleep(random_int(1, 2));
						}
					}
				}

				$maxId = $feed->getNextMaxId();

			} while ($maxId !== null && $count < $count_needed);

			if ($text_message !== "") {
				$this->i->directMessage($follow_id, $text_message);
			}
		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

	/**
	 * Like posts by location
	 *
	 * @param string $position
	 * @param int $count_needed
	 * @return int
	 */
	private function likeByLocation($position = '', $count_needed = 0, $user_filter = []) {
		$progress_message = '';

		if(empty($position) || empty($count_needed)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		list($lat, $lng) = explode(",", substr($position, 1, -1));

		try {
			Common::setRandomInstagramUser($this->i_random);
			$locations = $this->i_random->searchLocation($lat, $lng);
			$place_id = $locations->venues[0]->external_id;

			if(!$place_id){
				return ['result' => false, 'progress_message' => 'Empty params'];
			}

			$maxId = null;
			$count = 0;

			do {
				$feed = $this->i->getLocationFeed($place_id, $maxId);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if(!$feed_item->has_liked){
						if(!$this->additionalUserFilter($feed_item->user->pk, $user_filter)){
							continue;
						}

						$like_response = $this->i->like($feed_item->id);
						if($like_response->status == 'ok'){
							$count++;
							sleep(random_int(1, 2));
						}
					}
				}

				$maxId = $feed->getNextMaxId();
				sleep(random_int(1, 2));

			} while ($maxId !== null && $count < $count_needed);

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

	/**
	 * Comment posts by location
	 *
	 * @param string $position
	 * @param int $count_needed
	 * @param string $comment_text
	 * @return int Counter
	 */
	private function commentByLocation($position = '', $count_needed = 0, $comment_text = '', $user_filter = []) {
		$progress_message = '';

		if(empty($position) || empty($count_needed) || empty($comment_text)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		list($lat, $lng) = explode(",", substr($position, 1, -1));

		try {
			Common::setRandomInstagramUser($this->i_random);
			$locations = $this->i_random->searchLocation($lat, $lng);
			$place_id = $locations->venues[0]->external_id;

			if(!$place_id){
				return ['result' => false, 'progress_message' => 'Empty params'];
			}

			$maxId = null;
			$count = 0;

			do {
				$feed = $this->i->getLocationFeed($place_id, $maxId);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if($feed_item->comments_disabled){
						continue;
					}

					// if already commented - go to next post
					foreach ($feed_item->preview_comments as $comment){
						if($comment->user_id == $this->i->account_id){
							continue 2;
						}
					}

					if(!$this->additionalUserFilter($feed_item->user->pk, $user_filter)){
						continue;
					}

					$comment_response = $this->i->comment($feed_item->id, $comment_text);
					if($comment_response->status == 'ok'){
						$count++;
						sleep(random_int(1, 2));
					}

				}

				$maxId = $feed->getNextMaxId();

			} while ($maxId !== null && $count < $count_needed);

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

	/**
	 * Like posts of username
	 *
	 * @param string $username
	 * @param int $count_needed
	 * @return boolean
	 */
	private function likeByUsername($username = '', $count_needed = 0) {
		$progress_message = '';

		if(empty($username) || empty($count_needed)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		try {

			$maxId = null;
			$count = 0;

			do {
				$id = $this->i->getUsernameId($username);
				$feed = $this->i->getUserFeed($id);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if(!$feed_item->has_liked){
						$like_response = $this->i->like($feed_item->id);
						if($like_response->status == 'ok'){
							$count++;
							sleep(random_int(1, 2));
						}
					}
				}

				$maxId = $feed->getNextMaxId();
				sleep(random_int(1, 2));

			} while ($maxId !== null && $count < $count_needed);

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

	/**
	 * Like posts by hashtag
	 *
	 * @param string $hashtag
	 * @param int $count_needed
	 * @return int Counter
	 */
	private function likeByHashTag($hashtag = '', $count_needed = 0, $user_filter = []){
		$progress_message = '';

		if(empty($hashtag) || empty($count_needed)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		try {
			$maxId = null;
			$count = 0;

			do {
				$feed = $this->i->getHashtagFeed($hashtag, $maxId);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if(!$feed_item->has_liked){

						if(!$this->additionalUserFilter($feed_item->user->pk, $user_filter)){
							continue;
						}

						$like_response = $this->i->like($feed_item->id);
						if($like_response->status == 'ok'){
							$count++;
							sleep(random_int(1, 2));
						}
					}
				}

				$maxId = $feed->getNextMaxId();
				sleep(random_int(1, 2));

			} while ($maxId !== null && $count < $count_needed);

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

	/**
	 * Comment posts by hashtag
	 *
	 * @param string $hashtag
	 * @param int $count_needed
	 * @param string $comment_text
	 * @return int Counter
	 */
	private function commentByHashTag($hashtag = '', $count_needed = 0, $comment_text = '', $user_filter = []) {
		$progress_message = '';

		if(empty($hashtag) || empty($count_needed) || empty($comment_text)){
			return ['result' => false, 'progress_message' => 'Empty params'];
		}

		try {

			$maxId = null;
			$count = 0;

			do {
				Common::setRandomInstagramUser($this->i_random);
				$feed = $this->i_random->getHashtagFeed($hashtag, $maxId);

				foreach ($feed->items as $feed_item){

					if($count == $count_needed){
						break;
					}

					if($feed_item->comments_disabled){
						continue;
					}

					// if already commented - go to next post
					foreach ($feed_item->preview_comments as $comment){
						if($comment->user_id == $this->i->account_id){
							continue 2;
						}
					}

					if(!$this->additionalUserFilter($feed_item->user->pk, $user_filter)){
						continue;
					}

					$comment_response = $this->i->comment($feed_item->id, $comment_text);
					if($comment_response->status == 'ok'){
						$count++;
						sleep(random_int(1, 2));
					}

				}

				$maxId = $feed->getNextMaxId();

			} while ($maxId !== null && $count < $count_needed);

		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}


	/**
	 *
	 * @param type $user_pk
	 * @param type $username
	 * @param type $additional_filter
	 * @return boolean
	 */
	private function additionalUserFilter($user_pk = 0, $additional_filter = []) {

		if(empty($additional_filter)){
			return true;
		}

		$followers_min = isset($additional_filter["followers_min"]) ? intval($additional_filter["followers_min"]) : 0;
		$followers_max = isset($additional_filter["followers_max"]) ? intval($additional_filter["followers_max"]) : 10000000000;
		$followed_min =	isset($additional_filter["followed_min"]) ? intval($additional_filter["followed_min"]) : 0;
		$followed_max = isset($additional_filter["followed_max"]) ? intval($additional_filter["followed_max"]) : 10000000000;
		$count_media_min = isset($additional_filter["count_media_min"]) ? intval($additional_filter["count_media_min"]) : 0;
		$count_media_max = isset($additional_filter["count_media_max"]) ? intval($additional_filter["count_media_max"]) : 10000000000;

		try {
			Common::setRandomInstagramUser($this->i_random);
			$user_info = $this->i_random->getUserInfoById($user_pk);

			if($user_info->status != 'ok'){
				return false;
			}

			if ($user_info->user->follower_count < $followers_min || $user_info->user->follower_count > $followers_max ||
				$user_info->user->following_count < $followed_min || $user_info->user->following_count > $followed_max ||
				$user_info->user->media_count < $count_media_min || $user_info->user->media_count > $count_media_max) {

				return false;
			}

			if (isset($additional_filter["photo"]) && $additional_filter["photo"] === "Yes") {
				if ($user_info->user->has_anonymous_profile_picture == 1) {
					return false;
				}
			}
			if (isset($additional_filter["count_day_last_media"])) {
				$feed = $this->i_random->getUserFeed($user_info->user->pk);
				$feeds = $feed->items;
				$last_publication = $feeds[0]->device_timestamp;
				$filter_time = (int) microtime(true) * 1000;
				$filter_time = $filter_time - (($additional_filter["count_day_last_media"] * 24) * 60 * 60 * 1000);
				if ($last_publication < $filter_time) {
					return false;
				}
			}
			if (isset($additional_filter["stop_list"])) {
				$biografy = $user_info->user->biography;
				$full_name = $user_info->user->full_name;
				$array_words = explode(",", $additional_filter["stop_list"]);
				foreach ($array_words as $key => $value) {
					$value = trim($value);
					if (!empty($biografy)) {
						if (strripos($biografy, $value)) {
							return false;
						}
					}
					if (!empty($full_name)) {
						if (strripos($full_name, $value)) {
							return false;
						}
					}
				}
			} elseif (isset($additional_filter["white_list"])) {
				$in_white_list = false;
				$biografy = $user_info->user->biography;
				$full_name = $user_info->user->full_name;
				$array_words = explode(",", $additional_filter["white_list"]);
				foreach ($array_words as $key => $value) {
					$value = trim($value);
					if (!empty($biografy)) {
						if (strripos($biografy, $value)) {
							$in_white_list = true;
							break;
						}
					}
					if (!empty($full_name)) {
						if (strripos($full_name, $value)) {
							$in_white_list = true;
							break;
						}
					}
				}
				if (!$in_white_list) {
					return false;
				}
			}
		} catch (\Exception $e) {
			//echo 'Something went wrong: ' . $e->getMessage() . "\n";
			return false;
		}
		//echo "true";
		return true;
	}



	private function copy_followers_by_account($username, $count_needed) {

		$progress_message = '';

		try {
			Common::setRandomInstagramUser($this->i_random);
			$id = $this->i_random->getUsernameId($username);
			$follow_id = array();
			$maxId = null;
			$count = 0;
			do {
				Common::setRandomInstagramUser($this->i_random);
				$users = $this->i_random->getUserFollowers($id, $maxId);

				foreach ($users->users as $user_item) {

					if ($count == $count_needed) {
						break;
					}

					if (!$user_item->is_private) {
//						$follow_response = $this->i->follow($user_item->pk);
//						if($follow_response->status == 'ok'){
						$count++;
						$follow_id[] = $user_item->pk;
						sleep(random_int(1, 2));
//						}
					}
				}
				//print_r($follow_id);
				$maxId = $users->getNextMaxId();
			} while ($maxId !== null && $count < $count_needed);
			do {
				Common::setRandomInstagramUser($this->i_random);
				$users = $this->i_random->getUserFollowings($id, $maxId);

				foreach ($users->users as $user_item) {

					if ($count == $count_needed) {
						break;
					}

					if (!$user_item->is_private && !in_array($user_item->pk, $follow_id)) {
//						$follow_response = $this->i->follow($user_item->pk);
//						if($follow_response->status == 'ok'){
						$count++;
						$follow_id[] = $user_item->pk;
						sleep(random_int(1, 2));
//						}
					}
				}
				//print_r($follow_id);
				$maxId = $users->getNextMaxId();
			} while ($maxId !== null && $count < $count_needed);
			do {
				Common::setRandomInstagramUser($this->i_random);
				$feed = $this->i_random->getUserFeed($id, $maxId);

				foreach ($feed->items as $feed_item) {
					if ($count == $count_needed) {
						break;
					}
					Common::setRandomInstagramUser($this->i_random);
					$users_likes = $this->i_random->getMediaLikers($feed_item->id);
					foreach ($users_likes->users as $user) {
						if (!$user->is_private && !in_array($user->pk, $follow_id)) {

							if ($count == $count_needed) {
								break;
							}

//						$follow_response = $this->i->follow($user->pk);
//						if($follow_response->status == 'ok'){
							$count++;
							$follow_id[] = $user->pk;
							sleep(random_int(1, 2));
//						}
						}
					}
				}

				$maxId = $feed->getNextMaxId();
				sleep(random_int(1, 2));
			} while ($maxId !== null && $count < $count_needed);
			print_r($follow_id);
		} catch (\Exception $e) {
			$progress_message = $e->getMessage();
		}

		return ['result' => $count, 'progress_message' => $progress_message];
	}

//	public static function index() {
//		if (Auth::check()) {
//			$id = 3;
//			$filter["position"] = "(38.9001846,-77.04360008)";
//			$count = 10;
//			$filter["additional"]["follower"]["min"] = 300;
//			$filter["additional"]["following"]["min"] = 100;
//			$filter["additional"]["following"]["max"] = 200;
//			$filter["additional"]["media"]["min"] = 100;
//			$filter["additional"]["media"]["max"] = 200;
//			$user = DB::table('user_instagram')->where('id', $id)->first();
//			$i = new Instagram(false, true, [
//				"storage" => "mysql",
//				"dbhost" => env('DB_HOST', '192.168.1.100'),
//				"dbname" => env('DB_DATABASE', 'man'),
//				"dbusername" => env('DB_USERNAME', 'forge'),
//				"dbpassword" => env('DB_PASSWORD', ''),
//			]);
//			try {
//				$i->setUser($user->inst_name, $user->password);
//				$i->login();
//			} catch (\Exception $e) {
//				echo 'Something went wrong: ' . $e->getMessage() . "\n";
//				exit(0);
//			}
//			try {
//				$id=$i->getUsernameId("little_todd");
//				$val=$i->getUserFeed($id);
//				foreach ($val->items as $value){
//				print_r($value->user->friendship_status);
//				echo"<br>";
//				if($value->has_liked){
//					echo "+".$value->code."<br>";
//				}else{
//					echo "-".$value->code."<br>";
//				}
//				}
//
//			} catch (\Exception $e) {
//				echo 'Something went wrong: ' . $e->getMessage() . "\n";
//				exit(0);
//			}
//		} else {
//			return view('auth.login');
//		}
//	}
}
