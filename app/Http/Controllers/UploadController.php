<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller {

	public function getForm() {
		if (Auth::check()) {
			$accounts = Common::getAccountsMenu();
			return view('autopost', [
				'accounts' => $accounts['accounts'],
				'id' => $accounts['current_account_id'],
				'current_account_name' => $accounts['current_account_name'],
				'current_account_image' => $accounts['current_account_image'],
			]);
		} else {
			return view('auth.login');
		}
	}

	public function upload(Request $request) {
		if (Auth::check()) {
			$imgUrl = $_POST['imgUrl'];
// original sizes
			$imgInitW = $_POST['imgInitW'];
			$imgInitH = $_POST['imgInitH'];
// resized sizes
			$imgW = $_POST['imgW']*2;
			$imgH = $_POST['imgH']*2;
// offsets
			$imgY1 = $_POST['imgY1']*2;
			$imgX1 = $_POST['imgX1']*2;
// crop box
			$cropW = $_POST['cropW']*2;
			$cropH = $_POST['cropH']*2;
// rotation angle
			$angle = $_POST['rotation'];

			$jpeg_quality = 100;

			$name = time() .rand();
			$source["patch"] = 'images/' . Auth::user()->id . "/";
			$output_path = public_path() . '/images/' . Auth::user()->id . "/";
			if(!is_dir ($output_path)){
				mkdir($output_path, 0777);
			}
			$output_filename = $name;

// uncomment line below to save the cropped image in the same location as the original image.
//$output_filename = dirname($imgUrl). "/croppedImg_".rand();

			$what = getimagesize($imgUrl);

			switch (strtolower($what['mime'])) {
				case 'image/png':
					$img_r = imagecreatefrompng($imgUrl);
					$source_image = imagecreatefrompng($imgUrl);
					$type = '.png';
					break;
				case 'image/jpeg':
					$img_r = imagecreatefromjpeg($imgUrl);
					$source_image = imagecreatefromjpeg($imgUrl);
					error_log("jpg");
					$type = '.jpeg';
					break;
				case 'image/gif':
					$img_r = imagecreatefromgif($imgUrl);
					$source_image = imagecreatefromgif($imgUrl);
					$type = '.gif';
					break;
				default: die('image type not supported');
			}


//Check write Access to Directory

			if (!is_writable(dirname($output_path))) {
				$response = Array(
					"status" => 'error',
					"message" => 'Can`t write cropped File'
				);
			} else {

// resize the original image to size of editor
				$resizedImage = imagecreatetruecolor($imgW, $imgH);
				imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
// rotate the rezized image
				$rotated_image = imagerotate($resizedImage, -$angle, 0);
// find new width & height of rotated image
				$rotated_width = imagesx($rotated_image);
				$rotated_height = imagesy($rotated_image);
// diff between rotated & original sizes
				$dx = $rotated_width - $imgW;
				$dy = $rotated_height - $imgH;
// crop rotated image to fit into original rezized rectangle
				$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
				imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
				imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
// crop image into selected area
				$final_image = imagecreatetruecolor($cropW, $cropH);
				imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
				imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
// finally output png image
//imagepng($final_image, $output_filename.$type, $png_quality);
				imagejpeg($final_image, $output_path . $output_filename . $type, $jpeg_quality);
				$source["name"] = $name.$type;
				$response = Array(
					"status" => 'success',
					"url" => '/images/' . Auth::user()->id . "/" . $output_filename . $type,
					"data"=>serialize($source)
				);
			}
			print json_encode($response);
		} else {
			return view('auth.login');
		}
	}

	public function success(Request $request) {
		if (Auth::check()) {
			if ($request->isMethod('post')) {
				$all = $request->all();
				if (isset($all["id"]) && isset($all["path"]) && isset($all["time"])) {
					$path = explode(",", $all["path"]);
					foreach ($path as $p) {
						if ($p !== "") {
							DB::table('events')->insert([
								['user_id' => Auth::user()->id, 'user_instagram_id' => $all["id"], 'type' => "Autopost", 'filter' => serialize(array("path"=>unserialize($p),"text_photo"=>$all["text"])), 'date_execute' => $all["time"], 'progress' => "scheduled"]
							]);
						}
					}
					echo "Autopost sheduling success";
				} else {
					echo "Error";
				}
			}
		} else {
			return view('auth.login');
		}
	}

}
