<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (Auth::check()) {
			$accounts = Common::getAccountsMenu();
			return view('home', [
				'accounts' => $accounts['accounts'],
				'id' => $accounts['current_account_id'],
				'current_account_name' => $accounts['current_account_name'],
				'current_account_image' => $accounts['current_account_image'],
			]);
		} else {
			return view('auth.login');
		}
	}

	public function getusername($id)
    {
		if (Auth::check()) {
			$users = DB::table('user_instagram')->where('user_id', Auth::user()->id)->get();
			foreach ($users as $user) {
				if ($user->id == $id) {
					return $user->inst_name;
				}
			}
		} else {
			return view('auth.login');
		}
	}
}
