<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use InstagramAPI\Instagram;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GetYourEvent extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id) {
		if (Auth::check()) {
			$accounts = Common::getAccountsMenu();
			return view('home', [
				'accounts' => $accounts['accounts'],
				'id' => $accounts['current_account_id'],
				'current_account_name' => $accounts['current_account_name'],
				'current_account_image' => $accounts['current_account_image'],
			]);
		} else {
			return view('auth.login');
		}
	}

	public function Events_List() {
		if (Auth::check()) {
			$userss = DB::table('events')->join('user_instagram', 'user_instagram.id', '=', 'events.user_instagram_id')
					->where('events.user_id', Auth::user()->id)->whereIn('progress', ['in progress', 'scheduled'])->get();
			try {
				foreach ($userss as $keys => $values) {
					$new = new \stdClass();
					$new->id = $keys;
					$new->user_instagram_id = $values->inst_name;
					$new->type = $values->type;
					$new->progress_count = $values->progress_count;
					$new->progress_message = $values->progress_message;
					$filt = unserialize($values->filter);
					if (isset($filt["filter"])) {
						$new->filter = $filt["filter"] . ": " . $filt["filter_source"];
						$new->count = $filt["count"];
					} else {
						$new->filter = "None";
						$new->count = 1;
					}
					$new->date = date("d/m/Y H:i", $values->date_execute);
					$new->progress = $values->progress;
					$string[] = $new;
				}
				return response()->json($string);
			} catch (\Exception $e) {
				echo 'Something went wrong: ' . $e->getMessage() . "\n";
				exit(0);
			}
		} else {
			return view('auth.login');
		}
	}

	public function History_List() {
		if (Auth::check()) {
			$userss = DB::table('events')->join('user_instagram', 'user_instagram.id', '=', 'events.user_instagram_id')
					->where('events.user_id', Auth::user()->id)->whereIn('progress', ['finished', 'failed'])->get();

			try {
				foreach ($userss as $keys => $values) {
					$new = new \stdClass();
					$new->id = $keys;
					$new->user_instagram_id = $values->inst_name;
					$new->type = $values->type;
					$new->progress_count = $values->progress_count;
					$new->progress_message = $values->progress_message;
					$filt = unserialize($values->filter);
					if (isset($filt["filter"])) {
						$new->filter = $filt["filter"] . ":" . $filt["filter_source"];
						$new->count = $filt["count"];
					} else {
						$new->filter = "None";
						$new->count = 1;
					}
					$new->date = date("d/m/Y H:i", $values->date_execute);
					$new->date_finished = date("d/m/Y H:i", $values->date_finished);
					$new->progress = $values->progress;
					$string[] = $new;
				}
				return response()->json($string);
			} catch (\Exception $e) {
				echo 'Something went wrong: ' . $e->getMessage() . "\n";
				exit(0);
			}
		} else {
			return view('auth.login');
		}
	}

}
