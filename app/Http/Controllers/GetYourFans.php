<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
//use InstagramAPI\Instagram;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GetYourFans extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/*
	 * Show fans page
	 *
	 */
	public function fans($id) {

		if (!Auth::check()) {
			return view('auth.login');
		}

		$accounts = Common::getAccountsMenu();
		return view('fans', [
			'accounts' => $accounts['accounts'],
			'id' => $accounts['current_account_id'],
			'current_account_name' => $accounts['current_account_name'],
			'current_account_image' => $accounts['current_account_image'],
		]);
	}

	/*
	 * Show not fans page
	 *
	 */
	public function nofans($id) {
		if (!Auth::check()) {
			return view('auth.login');
		}

		$accounts = Common::getAccountsMenu();
		return view('nofans', [
			'accounts' => $accounts['accounts'],
			'id' => $accounts['current_account_id'],
			'current_account_name' => $accounts['current_account_name'],
			'current_account_image' => $accounts['current_account_image'],
		]);
	}
	/*
	 * Show invite message page
	 *
	 */
	public function message($id) {
		if (!Auth::check()) {
			return view('auth.login');
		}
		$accounts = Common::getAccountsMenu();
		return view('invite_message', [
			'accounts' => $accounts['accounts'],
			'id' => $accounts['current_account_id'],
			'current_account_name' => $accounts['current_account_name'],
			'current_account_image' => $accounts['current_account_image'],
		]);
	}
	/*
	 * Show mutual followers  page
	 *
	 */
	public function mutual_followers($id) {
		if (!Auth::check()) {
			return view('auth.login');
		}
		$accounts = Common::getAccountsMenu();
		return view('mutual_followers', [
			'accounts' => $accounts['accounts'],
			'id' => $accounts['current_account_id'],
			'current_account_name' => $accounts['current_account_name'],
			'current_account_image' => $accounts['current_account_image'],
		]);

	}

	/*
	 * Get fans or nofans
	 */
	public function FansList($fans_nofans, $id) {
		if (!Auth::check()) {
			return view('auth.login');
		}

		$user = DB::table('user_instagram')->where('id', $id)->first();

		try {
			$i = Common::newInstagram($user->inst_name, $user->password);

			$following_by_pk = Common::getMyFollowings($i);
			$followers_by_pk = Common::getMyFollowers($i);

			if($fans_nofans == 'fans'){
				$fans = array_diff_key($followers_by_pk, $following_by_pk);
			}elseif($fans_nofans == 'mutual'){
				$fans = array_intersect_key($followers_by_pk, $following_by_pk);
			}else{
				$fans = array_diff_key($following_by_pk, $followers_by_pk);
			}

			$return_fans = [];

			foreach ($fans as $pk => $user) {
				$new = new \stdClass();
				$new->id = $pk;
				$new->name = $user["name"];
				$new->picture = $user["picture"];
				$new->full_name = $user["full_name"];
				$return_fans[] = $new;
			}
			return response()->json($return_fans);

		} catch (\Exception $e) {
			echo 'Something went wrong: ' . $e->getMessage() . "\n";
			exit;
		}
	}

	/*
	 * Mass ajax unfollow
	 */
	public function UnFollow($id, Request $request) {
		if (!Auth::check()) {
			return view('auth.login');
		}

		set_time_limit(120);

		$user = DB::table('user_instagram')->where('id', $id)->first();
		$unfollow_accounts = $request->get('data');

		try {
			$i = Common::newInstagram($user->inst_name, $user->password);

			foreach ($unfollow_accounts as $account){
				$i->unfollow($account['id']);
				sleep(random_int(1, 3));
			}

			return "Unfollowed ".count($unfollow_accounts). ' users';
		} catch (\Exception $e) {
			return 'Something went wrong: ' . $e->getMessage() . "\n";
		}
	}

	/*
	 * Get followers
	 */
	public function Followers($id) {
		if (!Auth::check()) {
			return view('auth.login');
		}

		$user = DB::table('user_instagram')->where('id', $id)->first();

		try {
			$i = Common::newInstagram($user->inst_name, $user->password);

			$following_by_pk = Common::getMyFollowers($i);

			foreach ($following_by_pk as $pk => $user) {
				$new = new \stdClass();
				$new->id = $pk;
				$new->name = $user["name"];
				$new->picture = $user["picture"];
				$new->full_name = $user["full_name"];
				$return_fans[] = $new;
			}
			return response()->json($return_fans);

		} catch (\Exception $e) {
			echo 'Something went wrong: ' . $e->getMessage() . "\n";
			exit;
		}
	}


	/*
	 * Mass ajax message
	 */
	public function Message_send(Request $request) {
		if (!Auth::check()) {
			return view('auth.login');
		}

		set_time_limit(120);
        $id= $request->get('id');
		$user = DB::table('user_instagram')->where('id', $id)->first();
		$message_accounts = $request->get('data');
		$text_message = $request->get('text');
		$arr_id=array();

		try {
			$i = Common::newInstagram($user->inst_name, $user->password);

			foreach ($message_accounts as $account){
				$arr_id[]=$account['id'];
			}

			$i->directMessage($arr_id,$text_message);

			return 'Message success send';
		} catch (\Exception $e) {
			return 'Something went wrong: ' . $e->getMessage() . "\n";
		}
	}

}
