<?php

use Illuminate\Database\Seeder;

class ClearDB extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::dropIfExists('password_resets');
		Schema::dropIfExists('permission_role');
		Schema::dropIfExists('role_user');
		Schema::dropIfExists('roles');
		Schema::dropIfExists('items');
		Schema::dropIfExists('permissions');
		Schema::dropIfExists('users');
		DB::table('migrations')->truncate();
	}

}
