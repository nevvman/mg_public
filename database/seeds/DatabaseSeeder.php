<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$permission = [
			[
				'name' => 'role-list',
				'display_name' => 'Display Role Listing',
				'description' => 'See only Listing Of Role'
			],
			[
				'name' => 'role-create',
				'display_name' => 'Create Role',
				'description' => 'Create New Role'
			],
			[
				'name' => 'role-edit',
				'display_name' => 'Edit Role',
				'description' => 'Edit Role'
			],
			[
				'name' => 'role-delete',
				'display_name' => 'Delete Role',
				'description' => 'Delete Role'
			],
			[
				'name' => 'item-list',
				'display_name' => 'Display Item Listing',
				'description' => 'See only Listing Of Item'
			],
			[
				'name' => 'item-create',
				'display_name' => 'Create Item',
				'description' => 'Create New Item'
			],
			[
				'name' => 'item-edit',
				'display_name' => 'Edit Item',
				'description' => 'Edit Item'
			],
			[
				'name' => 'item-delete',
				'display_name' => 'Delete Item',
				'description' => 'Delete Item'
			]
		];

		foreach ($permission as $value) {
			Permission::create($value);
		}
        
		$root = new Role();
		$root->name = 'root';
		$root->display_name = 'Root';
		$root->description = 'Super user have all permission'; 
		$root->save();
		
		$admin = new Role();
		$admin->name = 'admin';
		$admin->display_name = 'User Administrator'; // optional
		$admin->description = 'User is allowed to manage and edit other users'; // optional
		$admin->save();

		$user = new Role();
		$user->name = 'User';
		$user->display_name = 'User '; 
		$user->description = 'Custom user'; 
		$user->save();

		$user_admin = User::create(array('name' => 'Admin', 'email' => 'admin@admin.com', 'password' => bcrypt('qwe012')));

		$user_admin->attachRole($admin);

		$admin->attachPermissions(array(1, 2, 3, 4, 5, 6, 7, 8));
		$user->attachPermissions(array(5,6,7,8));
	}

}
