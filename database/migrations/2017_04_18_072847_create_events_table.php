<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
		    $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('user_instagram_id')->unsigned();
			$table->string('type');
			$table->string('filter');
			$table->integer('date_execute');
			$table->integer('date_finished');
			$table->string('progress');
			$table->integer('progress_count');
			$table->string('progress_message');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_instagram_id')->references('id')->on('user_instagram')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
