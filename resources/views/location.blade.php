@extends('layouts.app')


@section('title')
Location Managrow
@endsection

@section('content')
<style>
	/* Always set the map height explicitly to define the size of the div
	 * element that contains the map. */
	#map {
        height: 500px;
	}
</style>
<div id="map"></div>
<div class="portlet box blue-dark col-md-12 collapse" id="instagram" style="margin-top: 10px">
	<div class="portlet-title">
		<div class="caption">Select an account to following: </div>
	</div>
	<div class="portlet-body">
		@foreach ($accounts as $user)
		<a href="#" class="accounts btn blue-dark btn-outline" data-instid="{{ $user->id }}"> {{ $user->inst_name }} </a>
		@endforeach
		<a href="/profile" class="btn blue-dark btn-outline"> Add New </a>
	</div>
</div>
<div class="portlet box blue-dark col-md-12 collapse" id="action">
	<div class="portlet-title">
		<div class="caption">Select count followings: </div>
	</div>
	<div class="portlet-body">
		<input type="number" id="count_follows" min="1" max="500">
		<button class="btn blue-dark btn-outline" id="submit" style="margin-left: 3%;width:10%;display: none;">OK</button>
	</div>
</div>
<input type="hidden" id="inst_data" name="inst_id" value="">
<input type="hidden" id="action_select" name="action_select" value="">
{{ csrf_field() }}

<script>
	$(function () {
		$(".accounts").click(function () {
			$("#action").collapse();
			$("#inst_data").val($(this).data("instid"));
			$(".accounts").removeClass('active');
			$(this).addClass("active");
		});
		$("#count_follows").keyup(function () {
			$("#submit").show();
			var value = $(this).val();
			if (value > 500) {
				value = 500;
				$(this).val(value);
				alert("Limit count 500");
			}
			if (value < 1) {
				value = 1;
				$(this).val(value);
				alert("Limit count 500");
			}
			$("#action_select").val(value);
			$(".action").removeClass('active');
			$(this).addClass("active");
		});
		$("#submit").click(function () {
			var _token = $("input[name=_token]").val();
			var id = $("#inst_data").val();
			var action = $("#action_select").val();
			$.post("/locations/new", {id: id, action: action, position:localStorage.getItem("latLng"), _token: _token})
					.done(function (data) {
						$("#Ajax").html(data);
						$("#ajax_block").collapse();

			});
		});
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6qL7b96Ba0yz2lLcSIEpabqgu4vuTHLI&callback=initMap"
async defer></script>
<script>
	var map;
	var marker;
	var infowindow;
	var infowindowmain;
	function initMap() {
		localStorage.removeItem("click");
		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -34.397, lng: 150.644},
			zoom: 18
		});
		google.maps.event.addListener(map, "click", function (e) {
			$("#instagram").collapse("show");
			if (localStorage.getItem('click')) {
				marker.setPosition(e.latLng);
			} else {
				addMarker(e.latLng, map);
				localStorage.setItem("click", true);
			}
			localStorage.setItem('latLng', e.latLng);
			var latLng = e.latLng;
			if (infowindow == null) {
				infowindow = new google.maps.InfoWindow({
					content: 'Wait for loading <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">'
				});
				infowindow.open(map, marker);
			} else {
				infowindow.setContent('Wait for loading <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">');
			}
			$.get("/locations/{{$id}}/" + latLng.lat() + "/" + latLng.lng(), function (data) {
				infowindow.setContent("Count Posts " + data);
			});
		});

		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				map.setCenter(pos);
			}, function () {
			});
		} else {
			;
		}
	}
	function addMarker(location, map) {
		marker = new google.maps.Marker({
			position: location,
			map: map,
			draggable: true,
			animation: google.maps.Animation.DROP
		});
		google.maps.event.addListener(marker, 'dragend', function (e) {
			localStorage.setItem('latLng', e.latLng);
			var latLng = e.latLng;
			if (infowindow == null) {
				infowindow = new google.maps.InfoWindow({
					content: 'Wait for loading <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">'
				});
				infowindow.open(map, marker);
			} else {
				infowindow.setContent('Wait for loading <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">');
			}
			$.get("/locations/{{$id}}/" + latLng.lat() + "/" + latLng.lng(), function (data) {
				infowindow.setContent("Count Posts " + data);
			});
		});
		google.maps.event.addListener(marker, "click", function (e) {
			localStorage.setItem('latLng', e.latLng);
			var latLng = e.latLng;
			if (infowindow == null) {
				infowindow = new google.maps.InfoWindow({
					content: 'Wait for loading <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">'
				});
				infowindow.open(map, marker);
			} else {
				infowindow.setContent('Wait for loading <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">');
			}
			$.get("/locations/{{$id}}/" + latLng.lat() + "/" + latLng.lng(), function (data) {
				infowindow.setContent("Count Posts " + data);
			});
		});
	}
	google.maps.event.addDomListener(window, 'load', initMap);
</script>

@endsection
