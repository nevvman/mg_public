
@extends('layouts.app')

@section('title')
Not Fans
@endsection

@section('content')
<style>
	#table .col1{
		width:20%;
		vertical-align: middle;
		border: none;
		text-align: center;
	}
	#table .col2{
		vertical-align: middle;
		width:40%;
		border: none;
	}
	#table .fan-check{
		vertical-align: middle;
		border: none;
	}
	.portlet.box > .portlet-body{
		padding-top: 0;
	}
	.fan_pk{
		display: none;
		padding-top: 0;
	}
</style>
<div class="col-md-12">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="portlet box blue-dark">
		<div class="portlet-title">
			<div class="caption">
				Users that don't follow you back
			</div>
		</div>
		<div class="portlet-body">
			<div class="table-responsive fixed-table-container table-no-bordered">
				<div class="returnAjax note note-success collapse"></div>
				<table id="table" data-toggle="table"
					   data-url="/nofans/{{$id}}/list"
					   data-pagination="true"
					   data-search="true"
					   data-height="675" class="table table-striped table-hover">
					<thead>
						<tr>
							<th data-field="id" class="fan_pk"></th>
							<th data-field="state" data-checkbox="true" class="fan-check"></th>
							<th data-field="picture" data-formatter="imageFormatter" class="col1">Picture</th>
							<th data-field="name" data-formatter="nameFormatter" class="col2">Username</th>
							<th data-field="full_name" class="col2">Full Name</th>
						</tr>
					</thead>
				</table>
				{{ csrf_field() }}
			</div>
		</div>
	</div>
</div>
<script>

	function imageFormatter(value, row) {
		return '<img class="img-circle" src="' + value + '" alt="Profile picture" width="35" height="35">';
	}
	function nameFormatter(value, row) {
		return '<a href="https://instagram.com/' + value + '" target="_blank">@' + value + '</a>';
	}

	(function ($) {
		setTimeout(function () {
			$(".fixed-table-toolbar").prepend('<div class="pull-left" style="padding: 15px;display: -webkit-inline-box;"><a href="#" class="unfollow btn blue-dark btn-outline"> UNFOLLOW </a><img src="/assets/loader.gif" class="loading collapse" alt="Please wait" style="width:20%;margin-left: 20px;"></div>');
			$(".unfollow").click(function () {
				//console.log('eeee');
				var data = $("#table").bootstrapTable("getSelections");
				//console.log(data);
				//return;

				//var id = "";
				if (data.length == 0) {
					alert("You have not select users");
				} else {

					$(".loading").collapse("show");
					$(".unfollow").addClass("disabled");
					$.post("/nofans/{{$id}}/unfollow", {data: data, _token: $("input[name=_token]").val()})
							.done(function (data) {
								$(".loading").collapse("hide");
								$(".returnAjax").html(data);
								$(".returnAjax").collapse("show");
								$("#table").bootstrapTable('refresh');
								setTimeout(function () {
									$(".returnAjax").collapse("hide");
									$(".unfollow").removeClass( "disabled" );
								}, 1500);
							});
				}
			});
		}, 500);

		$.fn.bootstrapTable.locales['en-US-custom'] = {
			formatLoadingMessage: function () {
				return '<img src="/assets/loader.gif" width="50" heigth="50" alt="Please wait.... " style="margin-top:200px;">';
			},
			formatRecordsPerPage: function (pageNumber) {
				return pageNumber + ' users per page';
			},
			formatShowingRows: function (pageFrom, pageTo, totalRows) {
				return 'Showing ' + pageFrom + ' to ' + pageTo + ' of ' + totalRows + ' users';
			},
			formatSearch: function () {
				return 'Search';
			},
			formatNoMatches: function () {
				return 'No matching users found';
			},
			formatPaginationSwitch: function () {
				return 'Hide/Show pagination';
			},
			formatRefresh: function () {
				return 'Refresh';
			},
			formatToggle: function () {
				return 'Toggle';
			},
			formatColumns: function () {
				return 'Columns';
			},
			formatAllRows: function () {
				return 'All';
			}
		};

		$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['en-US-custom']);
		var $result = $('#eventsResult');
		var all_foolow = "";
		localStorage.removeItem('follow');
		$('#table').on('check.bs.table', function (e, row) {
			if (localStorage.getItem("follow") !== null) {
				var new_follow = localStorage.getItem("follow") + row.id + ",";
			} else {
				var new_follow = row.id + ",";
			}
			localStorage.setItem('follow', new_follow);
		})
				.on('uncheck.bs.table', function (e, row) {
					var new_follow = localStorage.getItem("follow");
					new_follow = new_follow.replace(row.id + ",", " ");
					localStorage.setItem('follow', new_follow);
				})
				.on('check-all.bs.table', function (e, row) {
					localStorage.setItem('follow', "all");
				})
				.on('uncheck-all.bs.table', function (e, row) {
					localStorage.removeItem('follow');
				});
	})(jQuery);

</script>

@endsection
