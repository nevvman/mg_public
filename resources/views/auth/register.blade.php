@extends('auth.head')
@section('body')
<body class=" login">
	<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="index.html">
			<img src="/assets/pages/img/logo-big.png" alt="" /> </a>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form class="register-form" action="{{route('register') }}" method="post"  style="display: block;">
			{{ csrf_field() }}
			<h3 class="font-green">Sign Up</h3>
			<p class="hint"> Enter your personal details below: </p>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Name</label>
				<input class="form-control placeholder-no-fix {{ $errors->has('name') ? ' border-red-thunderbird' : '' }}" type="text" placeholder="Name" name="name" value="{{ old('name') }}"> 
				@if ($errors->has('name'))
				<span class="help-block bg-red-thunderbird bg-font-red-thunderbird text-center">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<input class="form-control placeholder-no-fix {{ $errors->has('email') ? ' border-red-thunderbird' : '' }}" type="text" placeholder="Email" name="email"> 
				@if ($errors->has('email'))
				<span class="help-block bg-red-thunderbird bg-font-red-thunderbird text-center">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<input class="form-control placeholder-no-fix {{ $errors->has('password') ? ' border-red-thunderbird' : '' }}" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password">
				@if ($errors->has('password'))
				<span class="help-block bg-red-thunderbird bg-font-red-thunderbird text-center">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation"> </div>
			<div class="form-actions">
				<a href="<?=$_SERVER['HTTP_REFERER']?>" id="register-back-btn" class="btn green btn-outline">Back</a>
				<button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
			</div>
		</form>
	</div>
</body>
@endsection
