@extends('auth.head')
@section('body')
<body class=" login">
	<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="index.html">
			<img src="/assets/pages/img/logo-big.png" alt="" /> </a>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form class="login-form" role="form" method="POST" action="{{ route('login') }}">

			{{ csrf_field() }}
			<h3 class="form-title font-green">Sign In</h3>
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				<span> Enter any username and password. </span>
			</div>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<input class="form-control form-control-solid placeholder-no-fix {{ $errors->has('email') ? 'border-red-thunderbird' : '' }}" type="text" autocomplete="on" placeholder="Username" name="email" /> 
			@if ($errors->has('email'))
			<span class="help-block bg-red-thunderbird bg-font-red-thunderbird text-center">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
			@endif
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<input class="form-control form-control-solid placeholder-no-fix {{ $errors->has('password') ? 'border-red-thunderbird' : '' }}" type="password" autocomplete="off" placeholder="Password" name="password" /> 
			@if ($errors->has('password'))
			<span class="help-block bg-red-thunderbird bg-font-red-thunderbird text-center">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
			@endif
			</div>
			<div class="form-actions">
				<button type="submit" class="btn green uppercase">Login</button>
				<label class="rememberme check mt-checkbox mt-checkbox-outline">
					<input type="checkbox" name="remember" value="1" {{ old('remember') ? 'checked' : '' }} />Remember
					<span></span>
				</label>
				<a href="{{ route('password.request') }}" id="forget-password" class="forget-password">Forgot Password?</a>
			</div>
			<div class="login-options" style="display: none;">
				<h4>Or login with</h4>
				<ul class="social-icons">
					<li>
						<a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
					</li>
					<li>
						<a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
					</li>
					<li>
						<a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
					</li>
					<li>
						<a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
					</li>
				</ul>
			</div>
			<div class="create-account">
				<p>
					<a href="/register" id="register-btn" class="uppercase">Create an account</a>
				</p>
			</div>
		</form>
		<!-- END LOGIN FORM -->
	</div>
</body>
@endsection

