
@extends('layouts.app')

@section('title')
Fans
@endsection

@section('content')
<style>
	#table .col1{
		width: 20%;
		vertical-align: middle;
		text-align: center;
		border: none;
	}
	#table .col2{
		vertical-align: middle;
		width: 40%;
		border: none;
	}
	.portlet.box > .portlet-body{
		padding-top: 0;
	}
</style>
<div class="col-md-12">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="portlet box blue-dark">
		<div class="portlet-title">
			<div class="caption">
				Followers that you don't follow back
			</div>
		</div>
		<div class="portlet-body">
			<div class="table-responsive fixed-table-container table-no-bordered">
				<table id="table" data-toggle="table"
					   data-url="/fans/{{$id}}/list"
					   data-pagination="true"
					   data-search="true"
					   data-height="665" class="table table-striped table-hover">
					<thead>
						<tr>
							<th data-field="picture" data-formatter="imageFormatter" class="col1">Picture</th>
							<th data-field="name" data-formatter="nameFormatter" class="col2">Username</th>
							<th data-field="full_name" class="col2">Full Name</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	function imageFormatter(value, row) {
		return '<img class="img-circle" src="' + value + '" alt="Profile picture" width="35" height="35">';
	}
	function nameFormatter(value, row) {
		return '<a href="https://instagram.com/' + value + '" target="_blank">@' + value + '</a>';
	}
	(function ($) {
		$.fn.bootstrapTable.locales['en-US-custom'] = {
			formatLoadingMessage: function () {
				return '<img src="/assets/loader.gif" width="75" heigth="75" alt="Please wait.... " style="margin-top:200px;">';
			},
			formatRecordsPerPage: function (pageNumber) {
				return pageNumber + ' users per page';
			},
			formatShowingRows: function (pageFrom, pageTo, totalRows) {
				return pageFrom + ' to ' + pageTo + ' of ' + totalRows + ' users';
			},
			formatSearch: function () {
				return 'Search';
			},
			formatNoMatches: function () {
				return 'No matching users found';
			},
			formatPaginationSwitch: function () {
				return 'Hide/Show pagination';
			},
			formatRefresh: function () {
				return 'Refresh';
			},
			formatToggle: function () {
				return 'Toggle';
			},
			formatColumns: function () {
				return 'Columns';
			},
			formatAllRows: function () {
				return 'All';
			}
		};

		$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['en-US-custom']);
	})(jQuery);

</script>

@endsection