
@extends('layouts.app')

@section('title')
Dashboard Managrow
@endsection

@section('content')
<style>
	.col1{
		width:30%;
	}
	.col2{
		width:10%;
	}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<ul class="nav nav-tabs nav-justified" id="event_table">
	<li class="active"><a data-toggle="tab" href="#planned" onclick='$("#evnt").bootstrapTable("refresh")'>Planned Events</a></li>
	<li><a data-toggle="tab" href="#history" onclick='$("#hist").bootstrapTable("refresh")'>Events History</a></li>
</ul>
<div class="tab-content">
	<div id="planned" class="tab-pane fade in active">

		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box blue-dark">
			<div class="portlet-title">
				<div class="caption">
					Planned Events
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-scrollable">
					<table id="evnt" data-toggle="table"
						   data-url="/events/all/list"
						   data-pagination="true"
						   data-search="true"
						   data-height="600" class="table table-striped table-hover">
						<thead>
							<tr>
								<th data-field="user_instagram_id" class="col2">Account</th>
								<th data-field="type" class="col1">Type</th>
								<th data-field="filter" class="col1">Filter</th>
								<th data-field="count" class="">Count Needed</th>
								<th data-field="progress_count" class="">Count Done</th>
								<th data-field="date" class="col2">Scheduled Date</th>
								<th data-field="progress" class="col2">Progress</th>
							</tr>
						</thead>
					</table>
					<script>
						(function ($) {
							$.fn.bootstrapTable.locales['en-US-custom'] = {
								formatLoadingMessage: function () {
									return '<img src="/assets/loader.gif" width="75" heigth="75" alt="Please wait.... " style="margin-top:200px;">';
								},
								formatRecordsPerPage: function (pageNumber) {
									return pageNumber + ' events per page';
								},
								formatShowingRows: function (pageFrom, pageTo, totalRows) {
									return 'Showing ' + pageFrom + ' to ' + pageTo + ' of ' + totalRows + ' events';
								},
								formatSearch: function () {
									return 'Search';
								},
								formatNoMatches: function () {
									return 'No matching events found';
								},
								formatPaginationSwitch: function () {
									return 'Hide/Show pagination';
								},
								formatRefresh: function () {
									return 'Refresh';
								},
								formatToggle: function () {
									return 'Toggle';
								},
								formatColumns: function () {
									return 'Columns';
								},
								formatAllRows: function () {
									return 'All';
								}
							};

							$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['en-US-custom']);
						})(jQuery);

					</script>
				</div>
			</div>
		</div>
	</div>
	<div id="history" class="tab-pane fade">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box blue-dark">
			<div class="portlet-title">
				<div class="caption">
					Events History
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-scrollable">
					<table id="hist" data-toggle="table"
						   data-url="/history/list"
						   data-pagination="true"
						   data-search="true"
						   data-height="600" class="table table-striped table-hover">
						<thead>
							<tr>
								<th data-field="user_instagram_id" class="col2">Account</th>
								<th data-field="type" class="col1">Type</th>
								<th data-field="filter" class="col1">Filter</th>
								<th data-field="count" class="">Count Needed</th>
								<th data-field="progress_count" class="">Count Done</th>
								<th data-field="date" class="col2">Date</th>
								<th data-field="date_finished" class="col2">Date Finished</th>
								<th data-field="progress" class="col2">Progress</th>
								<th data-field="progress_message" class="col2">Result Message</th>
							</tr>
						</thead>
					</table>
					<script>
						(function ($) {
							$.fn.bootstrapTable.locales['en-US-custom'] = {
								formatLoadingMessage: function () {
									return '<img src="/assets/loader.gif" width="75" heigth="75" alt="Please wait.... " style="margin-top:200px;">';
								},
								formatRecordsPerPage: function (pageNumber) {
									return pageNumber + ' events per page';
								},
								formatShowingRows: function (pageFrom, pageTo, totalRows) {
									return 'Showing ' + pageFrom + ' to ' + pageTo + ' of ' + totalRows + ' events';
								},
								formatSearch: function () {
									return 'Search';
								},
								formatNoMatches: function () {
									return 'No matching events found';
								},
								formatPaginationSwitch: function () {
									return 'Hide/Show pagination';
								},
								formatRefresh: function () {
									return 'Refresh';
								},
								formatToggle: function () {
									return 'Toggle';
								},
								formatColumns: function () {
									return 'Columns';
								},
								formatAllRows: function () {
									return 'All';
								}
							};

							$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['en-US-custom']);
						})(jQuery);

					</script>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection