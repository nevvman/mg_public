@extends('layouts.app')
@section('title')
User Profile
@endsection
@section('content')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<div class="col-md-12">
	<div class="col-md-6">
		<div class="portlet box blue-dark">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i> Edit account </div>
			</div>
			<div class="portlet-body form">
				<form role="form" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-4 control-label">UserName</label>
							<div class="col-md-8">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="username  account" data-container="body"></i>
									<input type="text" id="name" class="form-control" value="{{ $name }}"> </div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="email" data-container="body" ></i>
									<input type="text" id="email" value="{{ $email }}" class="form-control"> </div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-8">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="pass" data-container="body"></i>
									<input type="text" id="pass" class="form-control"> </div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Password confirm</label>
							<div class="col-md-8">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="password confirmation" data-container="body"></i>
									<input type="text" id="pass_confirm" class="form-control"> </div>
							</div>
						</div>
						{{ csrf_field() }}
						<div id="AjaxEditReturn"></div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-4 col-md-8">
								<a href="#" id="edit" onclick="" class="btn blue-dark">Edit account</a>
							</div>
						</div>
					</div>
					<script>
$(function () {
	$("#edit").click(function () {
		var username = $("#name").val();
		var email = $("#email").val();
		var password = $("#pass").val();
		var password_confirm = $("#pass_confirm").val();
		var _token = $("input[name=_token]").val();
		$.post("/profile/edit", {name: username, email: email, password: password, _token: _token})
				.done(function (data) {
					$("#AjaxEditReturn").html(data);
				});
	});
});
					</script>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box blue-dark">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-comments"></i>Accounts table</div>
			</div>
			<div class="portlet-body">
				<div class="table-scrollable">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th> # </th>
								<th> Username </th>
								<th> Password </th>
								<th> Actions </th>
							</tr>
						</thead>
						<tbody>
							@foreach ($users as $user)
							<tr>
								<td> {{ $user->id }} </td>
								<td> {{ $user->inst_name }} </td>
								<td> {{ $user->password }} </td>
								<td>
									<a href="#" class="edit green-jungle" data-id="{{ $user->id }}"><i class="fa fa-edit" style="color: green"></i></a>
									<a href="#" class="delete red-thunderbird" data-id="{{ $user->id }}"><i class="fa fa-remove" style="color:red"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<form role="form" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-4 control-label">UserName</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="username instagram account" data-container="body"></i>
									<input type="text" id="username" class="form-control"> </div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="password instagram account" data-container="body"></i>
									<input type="text" id="password" class="form-control"> </div>
							</div>
						</div>
						<div id="AjaxReturn" style="text-align: center;"></div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-4 col-md-4">
								<a href="#" id="account" onclick="" class="btn blue-dark"><i class="fa fa-plus-square" style="color:white; margin-right: 10px;"></i>Add account</a>
								<img src="/assets/loader.gif" id="loader" width="25" alt="Loading please wait...." style="margin-left: 20px; display: none;">
							</div>
						</div>
					</div>
					<script>
						$(function () {
							$("#account").click(function () {
								var username = $("#username").val();
								var password = $("#password").val();
								if (username !== "" && password !== "") {
									$("#loader").show("slow");
									$(this).addClass('disabled');
									$.get("/auth/" + username + "/" + password, function (data) {
										$("#AjaxReturn").html(data);
										$("#account").removeClass("disabled");
										$("#loader").hide("slow");
										setTimeout(function () {
											location.reload();
										}, 1000);
									});
								}
							});
							$(".edit").click(function () {
								var id = $(this).data("id");
								bootbox.prompt({
									title: "Enter your new instagram password",
									inputType: 'password',
									callback: function (result) {
										if (result !== null && result !== "") {
											$.post("/profile/edit", {inst_id: id, instagram_password: result, _token: $("input[name=_token]").val()})
													.done(function (data) {
														setTimeout(function () {
															location.reload();
														}, 1000);
													});
										}
									}
								});
							});
							$(".delete").click(function () {
								var id = $(this).data("id");
								bootbox.confirm("Delete this account?", function (result) {
									if (result === true) {
										$.post("/profile/edit", {inst_id: id, action: "delete", _token: $("input[name=_token]").val()})
												.done(function (data) {
													setTimeout(function () {
														location.reload();
													}, 1000);
												});
									}
								}
								);
							});
						});
					</script>
				</form>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
@endsection