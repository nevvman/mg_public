@extends('layouts.app')
@section('title')
Upload Files
@endsection

@section('content')
<div id="ajax_block" class="custom-alerts alert alert-info collapse"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><div id="Ajax"></div></div>
<script src="/assets/croppic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css" />
<div class="col-md-6">
	<div class="portlet box blue-dark col-md-12">
		<div class="portlet-title">
			<div class="caption">
				<span style="font-weight: 900">1</span> Select an account to autopost: </div>
		</div>
		<div class="portlet-body">
			@foreach ($accounts as $user)
			<a href="#" class="accounts btn blue-dark btn-outline" data-instid="{{ $user->id }}"> {{ $user->inst_name }} </a>
			@endforeach
			<a href="/profile" class="btn blue-dark btn-outline"> Add New </a>
		</div>
	</div>
	<div class="portlet box blue-dark col-md-12 collapse" id="date_w" style="margin-left: 1%">
		<div class="portlet-title">
			<div class="caption"><span style="font-weight: 900">2</span>  Date execute autopost: </div>
		</div>
		<div class="portlet-body">
			<div class="form-group">
				<input type="text" class="col-md-6" id="datetimepicker"/>
			</div>
		</div>
	</div>
	<div class="portlet box blue-dark col-md-12 collapse" id="text" style="margin-left: 1%">
		<div class="portlet-title">
			<div class="caption"><span style="font-weight: 900">3</span>  Text to photo's: </div>
		</div>
		<div class="portlet-body" style="height: 50px">
			<div class="form-group">
				<input type="text" class="col-md-6" id="text_timeline"/>
			</div>
		</div>
	</div>
	<div class="col-md-12"><button class="btn blue-dark col-md-offset-9 col-md-3 btn-outline collapse" style='font-weight: 900;' id="submit">OK</button></div>
</div>
<div class="cropHeaderWrapper collapse" id="drop" style="background: #eef1f5;">
	<div id="croppic"></div>
</div>
<input type="hidden" id="date" name="date" value="{{time()}}">
<input type="hidden" id="acc_id" name="acc_id" value="">
<input type="hidden" id="path" name="path" value="">
<script>
var croppicHeaderOptions = {
	//uploadUrl:'img_save_to_file.php',
	cropData: {
		"_token": $("input[name=_token]").val()
	},
	cropUrl: '/autopost/upload',
	enableMousescroll: true,
	modal: false,
	processInline: true,
	loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
	onBeforeImgUpload: function () {
	},
	onAfterImgUpload: function () {
	},
	onImgDrag: function () {
	},
	onImgZoom: function () {
	},
	onBeforeImgCrop: function () {
	},
	onAfterImgCrop: function (data) {
		$(".croppedImg").css("width", "540px");
		$("#path").val(data.data);
		$("#text").collapse();
		$("#submit").collapse();
	},
	onReset: function () {
	},
	onError: function (errormessage) {
		console.log('onError:' + errormessage)
	}
};
var croppic = new Croppic('croppic', croppicHeaderOptions);

$('#datetimepicker').datetimepicker({
	inline: true,
	startDate: '0',
	startTime: '0',
	minDate: '0',
	mask: true,
	step: 10,
	onChangeDateTime: function (dp, $input) {
		var time = "\"" + dp.getTime() + "\"";
		var new_time = parseInt(time.substr(1, time.length - 5), 10);
		if (new_time > parseInt("{{time()}}", 10)) {
			$("#date").val(new_time);
		} else {
			alert("Date invalid");
		}
	}
});
$(function () {
	$(".accounts").click(function () {
		$("#date_w").collapse();
		$("#drop").collapse();
		$("#acc_id").val($(this).data("instid"));
		$(".accounts").removeClass('active');
		$(this).addClass("active");
	});
	$("#submit").click(function () {
		var _token = $("input[name=_token]").val();
		var id = $("#acc_id").val();
		var text=$("#text_timeline").val();
		$.post("/autopost/new", {id: id, time: $("#date").val(), path: $("#path").val(), text:text, _token: _token})
				.done(function (data) {
					$("#Ajax").html(data);
					$("#ajax_block").collapse();
					setTimeout(function () {
						location.reload();
					}, 1000);

				});
	});
});
</script>
@endsection