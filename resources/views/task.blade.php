@extends('layouts.app')

@section('title')
{{ __('Create Task') }}
@endsection

@section('content')
<style>
	#range .irs-bar-edge {
		display: none !important;
	}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.7/css/ion.rangeSlider.skinNice.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.7/js/ion.rangeSlider.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.7/css/ion.rangeSlider.css" />
<div id="ajax_block" class="custom-alerts alert alert-info collapse"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><div id="Ajax"></div></div>
<div class="portlet box blue-dark col-md-12">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">1</span> {{ __('Select an account to promote') }}
		</div>
	</div>
	<div class="portlet-body">
		@foreach ($accounts as $user)
		<a href="#" class="accounts btn blue-dark btn-outline" data-instid="{{ $user->id }}"> {{ $user->inst_name }} </a>
		@endforeach
		<a href="/profile" class="btn blue-dark btn-outline"> {{ __('Add New') }} </a>
	</div>
</div>
<div class="portlet box blue-dark col-md-12 collapse" id="action">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">2</span> {{ __('Select action') }}
		</div>
	</div>
	<div class="portlet-body">
		<a href="#" class="action btn blue-dark btn-outline" data-action="1"><i class="fa fa-user-plus"></i> {{ __('Follow') }}</a>
		<!--<a href="#" class="action btn blue-dark btn-outline" data-action="2"><i class="fa fa-user-times"></i> UnFollow</a>-->
		<a href="#" class="action btn blue-dark btn-outline" data-action="3"><i class="fa fa-heart"></i> {{ __('Like') }}</a>
		<a href="#" class="action btn blue-dark btn-outline" id="comment" data-action="4"><i class="fa fa-comment"></i> {{ __('Comment') }}</a>
		<!--		<a href="#" class="action btn blue-dark btn-outline" data-action="5">Like+Follow</a>
				<a href="#" class="action btn blue-dark btn-outline" data-action="6">Message</a>-->
	</div>
</div>
<div class="portlet box blue-dark col-md-12 collapse" id="source">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">3</span> {{ __('Select source') }} </div>
	</div>
	<div class="portlet-body">
		<!--<a href="#" class="source btn blue-dark btn-outline" data-action="1"><i class="fa fa-user"></i> User</a>-->
		<a href="#" class="source source_add btn blue-dark btn-outline" data-action="2"><i class="fa fa-hashtag"></i> {{ __('HashTag') }}</a>
		<a href="#" class="source source_add btn blue-dark btn-outline" data-action="3"><i class="fa fa-map-marker"></i> {{ __('Location') }}</a>
		<a href="#" class="source btn blue-dark btn-outline" data-action="4" id="follow_only" style="display: none;"><i class="fa fa-clone"></i> {{ __('Copy from account') }}</a>
	</div>
</div>
<div class="portlet box blue-dark col-md-3 collapse" id="source_additional">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">4</span> {{ __('Enter login of the source user (without @)') }} </div>
	</div>
	<div class="portlet-body">
        <div class="form-group">
			<label for="username">{{ __('Username') }}</label>
			<div class="input-group">
				<input type="text" class="form-control" id="username" placeholder="{{ __('Username without @') }}">
				<span class="input-group-addon">
					<i class="fa fa-user font-red"></i>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="portlet box blue-dark col-md-3 collapse" id="source_additional_hash">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">4</span> {{ __('Enter hashtag (without #)') }}
		</div>
	</div>
	<div class="portlet-body">
        <div class="form-group">
			<div class="input-group">
				<input type="text" class="form-control" id="hashtag" placeholder="{{ __('Hashtag without #') }}">
				<span class="input-group-addon">
					<i class="fa fa-user font-red"></i>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="portlet box blue-dark col-md-3 collapse" id="source_additional_location">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">4</span> {{ __('Location') }} </div>
	</div>
	<div class="portlet-body">
        <div class="form-group">
			<div id="map" style="height:300px;"></div>
		</div>
	</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6qL7b96Ba0yz2lLcSIEpabqgu4vuTHLI&callback=initMap" async defer></script>
<script>
var map;
var marker;
var infowindow;
var infowindowmain;
function initMap() {
	localStorage.removeItem("click");
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 18
	});
	google.maps.event.addListener(map, "click", function (e) {
		$("#instagram").collapse("show");
		if (localStorage.getItem('click')) {
			marker.setPosition(e.latLng);
		} else {
			addMarker(e.latLng, map);
			localStorage.setItem("click", true);
		}
		localStorage.setItem('latLng', e.latLng);
		var latLng = e.latLng;
		$("#source_add").val(latLng);
		if (infowindow == null) {
			infowindow = new google.maps.InfoWindow({
				content: '{{ __("Loading...") }} <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">'
			});
			infowindow.open(map, marker);
		} else {
			infowindow.setContent('{{ __("Loading...") }} <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">');
		}
		$.get("/locations/{{$id}}/" + latLng.lat() + "/" + latLng.lng(), function (data) {
			infowindow.setContent("{{ __('Posts') }}: " + data);
		});
	});

	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			map.setCenter(pos);
		}, function () {
		});
	}
}
function addMarker(location, map) {
	marker = new google.maps.Marker({
		position: location,
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP
	});
	google.maps.event.addListener(marker, 'dragend', function (e) {
		localStorage.setItem('latLng', e.latLng);
		var latLng = e.latLng;
		$("#source_add").val(latLng);
		if (infowindow == null) {
			infowindow = new google.maps.InfoWindow({
				content: '{{ __("Loading...") }} <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">'
			});
			infowindow.open(map, marker);
		} else {
			infowindow.setContent('{{ __("Loading...") }} <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">');
		}
		$.get("/locations/{{$id}}/" + latLng.lat() + "/" + latLng.lng(), function (data) {
			infowindow.setContent("{{ __('Posts') }}: " + data);
		});
	});
	google.maps.event.addListener(marker, "click", function (e) {
		localStorage.setItem('latLng', e.latLng);
		var latLng = e.latLng;
		$("#source_add").val(latLng);
		if (infowindow == null) {
			infowindow = new google.maps.InfoWindow({
				content: '{{ __("Loading...") }} <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">'
			});
			infowindow.open(map, marker);
		} else {
			infowindow.setContent('{{ __("Loading...") }} <img src="/assets/loader.gif" width="15" heigth="15" alt="loading">');
		}
		$.get("/locations/{{$id}}/" + latLng.lat() + "/" + latLng.lng(), function (data) {
			infowindow.setContent("{{ __('Posts') }}: " + data);
		});
	});
}
google.maps.event.addDomListener(window, 'load', initMap);
</script>

<div class="portlet box blue-dark col-md-3 collapse" id="date" style="margin-left: 1%">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">5</span> {{ __('When execute task') }}
		</div>
	</div>
	<div class="portlet-body">
        <div class="form-group">
			<input type="text" class="col-md-6" id="datetimepicker"/>
			<script>
				$('#datetimepicker').datetimepicker({
					inline: true,
					startDate: '0',
					minDate: '0',
					mask: true,
					step: 10,
					startTime: '0',
					onChangeDateTime: function (dp, $input) {
						$("#date_h").val(dp.getTime());
					}
				});
			</script>
		</div>
	</div>
</div>
<div class="col-md-5">
<div class="portlet box blue-dark col-md-12 collapse" id="range" style="margin-left: 2%">
	<div class="portlet-title">
		<div class="caption">
			<span style="font-weight: 900">6</span> {{ __('Number of actions') }}
		</div>
	</div>
	<div class="portlet-body">
		<input type="text" id="range_input">
		<script>
			$("#range_input").ionRangeSlider({
				min: 0,
				max: 20,
				from: 5,
				onChange: function (data) {
					$("#count").val(data.from);
				}
			});
		</script>
	</div>
</div>
	<div class="portlet box blue-dark col-md-12 collapse" id="invite_message" style="margin-left: 2%">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-envelope-square" style='font-size: 25px;'></i>{{ __('Invite message') }}</div>
	</div>
	<div class="portlet-body">
        <div class="form-group">
			<textarea class="form-control" rows="3" placeholder="{{ __('Your message') }}" id="message_area"></textarea>
		</div>
	</div>
</div>
</div>
<div class="portlet box blue-dark col-md-12 collapse" id="comment_text">
	<div class="portlet-title">
		<div class="caption">
			{{ __('Comment text') }}
		</div>
	</div>
	<div class="portlet-body">
        <div class="form-group">
			<textarea class="form-control" rows="3" placeholder="{{ __('Comment text') }}" id="comment_area"></textarea>
		</div>
	</div>
</div>
<input type="hidden" id="inst_data" name="inst_id" value="">
<input type="hidden" id="action_select" name="action_select" value="">
<input type="hidden" id="source_data" name="source" value="">
<input type="hidden" id="date_h" name="date" value="">
<input type="hidden" id="count" name="count" value="5">
<input type="hidden" id="comment_field" name="comment_field" value="">
<input type="hidden" id="message_field" name="message_field" value="">
<input type="hidden" id="source_add" name="source_data" value="">
<input type="hidden" id="filt_add" name="add_filt_data" value="">
{{ csrf_field() }}
<div class="col-md-12 collapse" style="margin-bottom:10px;" id="source_filter_add">
	<a data-toggle="pill" data-id="0" class="btn blue-dark btn-outline active filter" onclick='document.getElementById("forms").reset();' href="#no_filter">{{ __('Without filter') }}</a>
	<a data-toggle="pill" data-id="1" class="btn blue-dark btn-outline filter" href="#additional_filter">{{ __('Additional filter') }}</a>
</div>

<div class="tab-content">
    <div id="no_filter" class="tab-pane fade">

    </div>
    <div id="additional_filter" class="tab-pane fade">
		<div class="portlet box blue-dark col-md-12">
			<div class="portlet-title">
				<div class="caption"> {{ __('Additional user filter') }} </div>
			</div>
			<div class="portlet-body" style="min-height: 300px;">
				<form id="forms">
					<div class="col-md-6">
						<label>{{ __('Followers') }}</label>
						<div class="form-group">
							<div class="col-md-5">
								<input type="number"  class="form-control range_filter" data-type="min" name="followers_min" placeholder="MinVal" min="0" step="10" max="100000">
							</div>
							<div class="col-md-offset-1 col-md-6">
								<input type="number" class="form-control range_filter" data-type="max" name="followers_max" placeholder="MaxVal" min="0" step="10" max="100000">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<label>{{ __('Followed') }}</label>
						<div class="form-group">
							<div class="col-md-5">
								<input type="number"  class="form-control range_filter" data-type="min" name="followed_min" placeholder="MinVal" min="0" step="10" max="100000">
							</div>
							<div class="col-md-offset-1 col-md-6">
								<input type="number" class="form-control range_filter" data-type="max" name="followed_max" placeholder="MaxVal" min="0" step="10" max="100000">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<label>{{ __('Number of posts') }}</label>
						<div class="form-group">
							<div class="col-md-5">
								<input type="number" class="form-control range_filter" data-type="min" name="count_media_min" placeholder="MinVal" min="0" step="10" max="100000">
							</div>
							<div class="col-md-offset-1 col-md-6">
								<input type="number" class="form-control range_filter" data-type="max" name="count_media_max" placeholder="MaxVal" min="0" step="10" max="100000">
							</div>
						</div>
					</div>
					<div class="col-md-6" style="margin-left:-15px;">
						<div class="form-group">
							<div class="col-md-6">
								<label for="date">Last post</label>
								<input type="number" class="form-control" placeholder="Days Ago" name="count_day_last_media" data-type="min" id="dates" min="0" step="1" max="100000">
							</div>
							<div class="col-md-6">
								<label for="list">{{ __('User photo') }}</label>
								<select name="photo" class="form-control" id="list">
									<option>{{ __('Not important') }}</option>
									<option>{{ __('Yes') }}</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="stop">{{ __('Stop words at Instagram Bio field (comma separated)') }}</label>
							<textarea class="form-control words" name="stop_list"  data-type="stop" rows="5" id="stop" placeholder="{{ __('some,stop,words') }}"></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="white">{{ __('White words at Instagram Bio field (comma separated)') }}</label>
							<textarea class="form-control words" name="white_list" data-type="white" rows="5" id="white" placeholder="{{ __('some,white,words') }}"></textarea>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
<div class="col-md-12"><button class="btn blue-dark col-md-offset-5 col-md-2 btn-outline collapse" id="submit" style='font-weight: 900;'>{{ __('Add task') }}</button></div>
<script>
	$(function () {
		$(".filter").click(function () {
			$(".filter").removeClass('active');
			$(this).addClass("active");
			var filter = $(this).data("id");
			if (filter) {
			}
		});

		$(".accounts").click(function () {
			$("#action").collapse();
			$("#inst_data").val($(this).data("instid"));
			$(".accounts").removeClass('active');
			$(this).addClass("active");
		});
		$(".action").click(function () {
			$("#source").collapse();
			var action = $(this).data("action");
			if(action==4){
				$("#comment_text").collapse("show");
			}else{
				$("#comment_text").collapse("hide");
				$("#comment_text").val("");
				$("#comment_field").val("");
			}
			$(".source_add").hide();
			if (action == 1 || action == 3 || action == 4) {
				$(".source_add").show();
			}
			if (action==1){
				$("#follow_only").show();
				if($("#source_data").val()!==""){
					$("#invite_message").collapse("show");
				}
			}else{
				$("#invite_message").collapse("hide");
				$("#follow_only").hide();
			}
			if (action==3 || action==4){
				if($("#source_data").val()==1){
					$("#range").collapse("show");
				}
			}else{
				if($("#source_data").val()!==1){
					$("#range").collapse("hide");
				}
			}
			$("#action_select").val($(this).data("action"));
			$(".action").removeClass('active');
			$(this).addClass("active");
		});
		$(".source").click(function () {
			$("#source_add").val("");
			$("#hashtag").val("");
			$("#username").val("");
			var source = $(this).data("action");
			var action = $("#action_select").val();
			if(action==1){
			$("#invite_message").collapse("show");
			}
			if (source == 1) {
				$("#source_additional").collapse("show");
				if(action==3 || action==4){
					$("#range").collapse("show");
				}
				else{
					$("#range").collapse("hide");
				}
			} else {
				if(source!== 4){
				$("#source_additional").collapse("hide");}
			}
			if (source == 2) {
				$("#source_additional_hash").collapse("show");
				$("#range").collapse("show");
				$("#source_filter_add").collapse("show");
			} else {
				$("#source_additional_hash").collapse("hide");
			}
			if (source == 3) {
				$("#source_additional_location").collapse("show");
				google.maps.event.trigger(map, 'resize');
				$("#range").collapse("show");
				$("#source_filter_add").collapse("show");
			} else {
				$("#source_additional_location").collapse("hide");
			}
			if (source == 4) {
				$("#range").collapse("show");
				$("#source_filter_add").collapse("show");
				$("#source_additional").collapse("show");
			} else {
				if(source!== 1){
				$("#source_additional").collapse("hide");
				}
				else{
					$("#source_filter_add").collapse("hide");
				}
			}
			$("#submit").collapse();
			$("#date").collapse();
			$("#source_data").val($(this).data("action"));
			$(".source").removeClass('active');
			$(this).addClass("active");
		});
		$('#username').keyup(function () {
			$("#source_add").val($(this).val());
		});
		$('#hashtag').keyup(function () {
			$("#source_add").val($(this).val());
		});
		$('#comment_area').keyup(function () {
			$("#comment_field").val($(this).val());
		});
		$('#message_area').keyup(function () {
			$("#message_field").val($(this).val());
		});
		$('.words').bind('input propertychange', function () {
			if($(this).data("type")==="stop"){
				$("#white").val(" ");
			}else{
				$("#stop").val(" ");
			}
			console.log(this.value);
		});
		$("#submit").click(function () {
			var _token = $("input[name=_token]").val();
			var id = $("#inst_data").val();
			var action = $("#action_select").val();
			var source = $("#source_data").val();
			if(action==1 && source==1 || action==2 && source==1){
				var count=1;
			}else{
				var count=$("#count").val();
			}
			var source_add = $("#source_add").val();
			var date=$("#date_h").val();
			var current_date=new Date().getTime();
			console.log(date);
			console.log(current_date);
			if(date<current_date){
				date=current_date;
			}
			var data = {};
			$('#forms').find('input,select').each(function () {
				if($(this).val()!==""){
				data[this.name] = $(this).val();
				}
			});
			$(".words").each(function () {
				if($(this).val()!==""){
				data[this.name] = $(this).val();
				}
			});
			$.post("/task/new", {id: id, action: action, filter_add: data, source: source, source_additional: source_add, time: date, count: count, comment: $("#comment_field").val(), message:$("#message_field").val(), _token: _token})
					.done(function (data) {
						$("#Ajax").html(data);
						$("#ajax_block").collapse();
						setTimeout(function () {
							location.reload();
						}, 1000);

					});
		});
	});
</script>
@endsection
