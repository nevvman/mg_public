@extends('layouts.app')

@section('title')
Dashboard Managrow
@endsection


@section('content')

<div class="portlet box purple col-md-6">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Add instagram account </div>
		<div class="tools">
			<a href="" class="collapse" data-original-title="" title=""> </a>
			<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
			<a href="" class="reload" data-original-title="" title=""> </a>
			<a href="" class="remove" data-original-title="" title=""> </a>
		</div>
	</div>
	<div class="portlet-body form">
		<form role="form" class="form-horizontal">
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-4 control-label">UserName</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa fa-info-circle tooltips" data-original-title="username instagram account" data-container="body"></i>
							<input type="text" id="username" class="form-control"> </div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Choose account who followed</label>
					<div class="col-md-9">
						<select id="account_id" class="form-control">
							@foreach ($users as $user)
							<option value="{{ $user->id }}">{{ $user->inst_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div id="AjaxReturn"></div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-4 col-md-8">
						<a href="#" id="follow" onclick="" class="btn blue">Submit</a>
					</div>
				</div>
			</div>
			<script>
				$(function () {
					$("#follow").click(function () {
						var username = $("#username").val();
						var id = $( "#account_id" ).val();
						if (username !== "" && id !== "") {
							$.get("/follow/" + username + "/" + id, function (data) {
								$("#AjaxReturn").html(data);
							});
							console.log(username + id);
						}
					});
				});
			</script>
		</form>
	</div>
</div>
</div>


@endsection
