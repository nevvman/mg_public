<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title')</title>

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">

		<!-- Scripts -->
		<script>
			window.Laravel = {!! json_encode([
					'csrfToken' => csrf_token(),
			]) !!};
		</script>
		 <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

		<script type="text/javascript" src="/libs/moment.js"></script>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/style.css" rel="stylesheet" type="text/css" />
        <link href="/assets/croppic.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
	</head>

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white @if (array_key_exists('sidebar_closed', $_COOKIE) and $_COOKIE['sidebar_closed'] == '1') page-sidebar-closed @endif">
		<div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <img src="/assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
							@if (Auth::guest())
							<li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
							<li><a href="{{ route('register') }}">{{ __('Register') }}</a></li>
							@else
							<li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="fa fa-user"></i>
                                    <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
									<li>
                                        <a href="/profile">
											<i class="fa fa-user"></i> Profile
										</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
										   onclick="event.preventDefault(); 	document.getElementById('logout-form').submit();">
											<i class="icon-key"></i> {{ __('Log Out') }}
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
                                    </li>
                                </ul>
                            </li>
							@endif


                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
			<!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed @if (array_key_exists('sidebar_closed', $_COOKIE) and $_COOKIE['sidebar_closed'] == '1') page-sidebar-menu-closed @endif" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->

							<li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
									@if (!empty($current_account_image))
									<img class="img-circle" src="{{ $current_account_image }}" alt="no pic" width="30" height="30">
									@else
										<i class="fa fa-user" style="color:white;"></i>
									@endif
                                    <span class="title"> {{ $current_account_name }}</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
									@foreach ($accounts as $user)
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" onclick="Cookies.set('current_account', {{ $user->id }}); Cookies.remove('current_account_image'); location.reload();">
											<i class="fa fa-user" style="color:white;"></i>
                                            <span class="title">{{ $user->inst_name }}</span>
                                        </a>
                                    </li>
									@endforeach
                                </ul>
                            </li>

							<li class="nav-item start">
                                <a href="/" class="nav-link nav-toggle">
                                    <i class="fa fa-line-chart" style="color:white"></i>
                                    <span class="title">{{ __('Tasks') }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/task" class="nav-link nav-toggle">
                                    <i class="fa fa-plus-square" style="color:white"></i>
                                    <span class="title">{{ __('Create Task') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="/fans/{{ $id }}" class="nav-link nav-toggle">
                                    <i class="fa fa-users" style="color:white"></i>
                                    <span class="title">{{ __('Fans') }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/nofans/{{ $id }}" class="nav-link nav-toggle">
                                    <i class="fa fa-users" style="color:white"></i>
                                    <span class="title">{{ __('Not Fans') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="/mutual/{{ $id }}" class="nav-link nav-toggle">
                                    <i class="fa fa-users" style="color:white"></i>
                                    <span class="title">{{ __('Mutual Subscribers') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="/message/{{ $id }}" class="nav-link nav-toggle">
                                    <i class="fa fa-envelope" style="color:white; font-size: 15px;"></i>
                                    <span class="title">{{ __('Send Message') }}</span>
                                </a>
                            </li>
							<li class="nav-item start ">
                                <a href="/autopost/" class="nav-link nav-toggle">
                                    <i class="fa fa-plus-square" style="color:white"></i>
                                    <span class="title">{{ __('Autoposting') }}</span>
                                </a>
                            </li>
							<!--							<li class="nav-item start ">
															<a href="/locations" class="nav-link nav-toggle">
																<i class="fa fa-line-chart" style="color:white"></i>
																<span class="title">Location Count Posts</span>
															</a>
														</li>
														<li class="nav-item start ">
															<a href="/" class="nav-link nav-toggle">
																<i class="fa fa-history" style="color:white"></i>
																<span class="title">History Tasks</span>
															</a>
														</li>                         -->

						</ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> @yield('title')
                            <small></small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12">@yield('content')</div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>

			</div>
			<div class="page-footer">
                <div class="page-footer-inner"> 2017 &copy; Managrow
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>


		</div>

		<!-- BEGIN CORE PLUGINS -->
        <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
		<script src="/assets/layouts/layout/scripts/layout.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

	</body>
</html>
