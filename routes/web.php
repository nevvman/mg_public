<?php
if( \Config::get('app.debug'))
{
    \Artisan::call( 'view:clear' );
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index");

Route::get('/username/{id}', "HomeController@getusername");
Route::get('/events/all/list', "GetYourEvent@Events_List");
Route::get('/history/list', "GetYourEvent@History_List");
Route::get('/events/{id}', "GetYourEvent@index");
Route::get('/locations', "LocationMap@index");
Route::get('/cron', "Cron@index");
Route::get('/task', "Task@index");
Route::post('/task/new', "Task@New_Task");

Route::get('/profile', "Profile@index");
Route::post('/profile/edit', "Profile@edit");

Route::get('/fans/{id}', "GetYourFans@fans");
Route::get('/nofans/{id}', "GetYourFans@nofans");
Route::get('/mutual/{id}', "GetYourFans@mutual_followers");
Route::post('/nofans/{id}/unfollow', "GetYourFans@UnFollow");
Route::get('/message/{id}', "GetYourFans@message");
Route::post('/followers/message', "GetYourFans@Message_send");

Route::get('/auth/{username}/{password}', "InstagramAuth@index");

Route::get('/autopost/','UploadController@getForm');
Route::post('/autopost/upload','UploadController@upload');
Route::post('/autopost/new','UploadController@success');

Route::get('/copyusers/{id}/','CopyFollowerAnyAccount@index');
Route::get('/feed/{id}/','GetFeedbyfilter@index');
Route::get('/locations/{id}/{lat}/{lng}','locationsCountPost@index');
Route::post('/locations/new','LocationMap@new_follow');

Route::get('/followers/{id}/list', "GetYourFans@Followers");
Route::get('/{fans_nofans}/{id}/list', "GetYourFans@FansList");


Route::auth();
